# Docker environment development

This `docker-compose` sets up a fully working development environment with many well-used dev-oriented and usefull troubleshooting tools.

## Usage

Just start all the tools with `docker-compose up`!

## Provided tools

| Service      | Host web access           | Host access     | Guest access        | Credentials              |
|--------------|---------------------------|-----------------|---------------------|--------------------------|
| traefik      | http://traefik.localhost/ |                 |                     |                          |
| redis        |                           | localhost:6379  | redis:6379          |                          |
| memcached    |                           | localhost:11211 | memcached:11211     |                          |
| postgresql   |                           | localhost:5432  | postgres:5432       | postgres // postgres     |
| pgadmin      | http://pg.localhost/      |                 |                     | admin@localhost // admin |
| mariadb      |                           | localhost:3306  | mariadb:3306        | root // mariadb          |
| phpmyadmin   | http://pma.localhost/     |                 |                     |                          |
| ldap         |                           | localhost:389   | ldap:389            |                          |
| phpldapadmin | http://pla.localhost/     |                 |                     |                          |
| mailhog      | http://mailhog.localhost/ | localhost:1025  | mailhog:1025        |                          |
| matomo       | http://matomo.localhost/  |                 | http://matomo/      |                          |
| sentry       | http://sentry.localhost/  |                 | http://sentry:9000/ |                          |

### Traefik

[Traefik](https://docs.traefik.io/) is a docker oriented reverse-proxy, used to exposed internal docker web services to `localhost` on the host.

It's available on http://traefik.localhost/

### Redis

[Redis](https://redis.io/) is an in-memory data store.

It's available on standard `localhost:6379` from the host and `redis:6379` on guests.
There is no default password.

Config file is available on `./redis/redis.conf` and persisted data on `./volumes/redis/`.

### Memcached

[Memcached](https://memcached.org/) is an in-memory cache system.

It's available on standard `localhost:11211` from the host and `memcached:11211` on guests.

### PostgreSQL

[PostgreSQL](https://www.postgresql.org/) is the DBMS to use by default.

It's available on standard `localhost:5432` from the host and `postgres:5432` on guests.

Default root password is `postgres`.

Persisted data are on `./volumes/postgres/`.

### MariaDB / MySQL

[MariaDB](https://mariadb.org/) is another DBMS in case you can't use PostgreSQL.

It's available on standard `localhost:3306` from the host and `mariadb:3306` from guests.

Default root password is `mariadb`.

Persisted data are on `./volumes/mariadb/`.

### pgAdmin

[pgAdmin](https://www.pgadmin.org/) is a web application to browse PostgreSQL.

It's available on http://pg.localhost/.

Default credentials are `admin@localhost` with password `admin`.

### phpMyAdmin

[phpMyAdmin](https://www.phpmyadmin.net/) is a web application to browse MariaDB.

It's available on http://pma.localhost/.

### phpLDAPadmin

[phpLDAPadmin](http://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page) is a web application to browse LDAP.

It's available on http://pla.localhost/.

### MailHog

[MailHog](https://github.com/mailhog/MailHog) is a SMTP mail catcher and a web application to read catched email.

It's available on http://mailhog.localhost/ from the host and can receive email with SMTP on `localhost:1025` from the host and `mailhog:1025` from guests.

### Sentry

[Sentry](https://sentry.io/) is a system to record uncatched error from your code.

It's available on http://sentry.localhost/ from the host and on http://sentry:9000/ from guests.

Persisted data are on `./volumes/sentry/`.

### Matomo

[Matomo](https://matomo.org/) is a GPDR compliant analytics system.

It's available on http://matomo.localhost/ from the host and on http://matomo/ from guests.

Persisted data are on `./volumes/matomo/`.

## Initial setup
### Sentry

Initially, Sentry must be provisionned as follow:

```
docker-compose up -d sentry
docker-compose exec sentry sentry config generate-secret-key
echo 'SENTRY_SECRET_KEY=<sentry-secret-key>' >> .env
docker-compose restart sentry
docker-compose exec sentry sentry upgrade
<create an admin user here>
```
